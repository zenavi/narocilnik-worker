package com.example.smsdevovc;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface IJsonMobileAPI {

    @GET("mobile")
    Call<List<Reservation>> getReservations();


    @POST("mobile")
    Call<JsonObject> postIdMobile(@Body JsonObject jsonObject);
}
