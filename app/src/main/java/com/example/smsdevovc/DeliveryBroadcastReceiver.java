package com.example.smsdevovc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;



public class DeliveryBroadcastReceiver extends BroadcastReceiver {
    private static final String ACTION_SMS_DELIVERED = "SMS_DELIVERED";

    // When the SMS has been delivered
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();


        if (action.equals(ACTION_SMS_DELIVERED)) {

            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Bundle bundle = intent.getExtras();
                    if(bundle != null){
                        //Kle not nared post request na doblen string id iz bundla
                        String idReservation = bundle.getString("id_delivered");
                        if(idReservation != null) {
                            Toast.makeText(context, "SMS delivered to : " + idReservation, Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(context, "SMS not delivered",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

}