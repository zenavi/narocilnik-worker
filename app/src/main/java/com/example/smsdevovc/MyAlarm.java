package com.example.smsdevovc;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyAlarm extends BroadcastReceiver {


    private static String ACTION_SMS_SENT = "SMS_SENT";
    private static String ACTION_SMS_DELIVERED = "SMS_DELIVERED";
    private static int MAX_SMS_MESSAGE_LENGTH = 160;
    private static Context mContext;


    @Override
    public void onReceive(final Context context, Intent intent) {
        mContext = context;


        //instanca retrofita
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://43c87ce1.ngrok.io")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //api instanca
        IJsonMobileAPI api = retrofit.create(IJsonMobileAPI.class);
        Call<List<Reservation>> klic = api.getReservations();
        klic.enqueue(new Callback<List<Reservation>>() {
            @Override
            public void onResponse(Call<List<Reservation>> call, Response<List<Reservation>> response) {

                if(!response.isSuccessful()){
                    Toast.makeText(mContext,"response ni bil prejet GET ERR!",Toast.LENGTH_LONG).show();
                    return;
                }

                List<Reservation> reservations = response.body();
                int requestCode = 0;


                if(reservations.size() > 0){
                    for(Reservation rez: reservations){
                        //

                        String phoneNumber = rez.getPhone();
                        String message = rez.getAvtSms();
                        //poslan
                        Intent sendIntent = new Intent(ACTION_SMS_SENT);
                        sendIntent.putExtra("id_sent", rez.getId());
                        //deliveran
                        Intent deliveredIntent = new Intent(ACTION_SMS_DELIVERED);
                        deliveredIntent.putExtra("id_delivered",rez.getId());
                        //intenta
                        PendingIntent piSent = PendingIntent.getBroadcast(mContext, ++requestCode, sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        PendingIntent piDelivered = PendingIntent.getBroadcast(mContext, ++requestCode, deliveredIntent,PendingIntent.FLAG_UPDATE_CURRENT);

                        //pošilanje sma

                        int length = message.length();
                        if (length > MAX_SMS_MESSAGE_LENGTH) {
                            SmsManager smsManager = SmsManager.getDefault();
                            ArrayList< String > messagelist = smsManager.divideMessage(message);
                            ArrayList<PendingIntent> sentIntents = new ArrayList<>();
                            sentIntents.add(piSent);
                            ArrayList<PendingIntent> deliveredIntents = new ArrayList<>();
                            deliveredIntents.add(piDelivered);
                            smsManager.sendMultipartTextMessage(phoneNumber, null, messagelist, sentIntents, deliveredIntents);
                        } else {
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(phoneNumber, null, message, piSent, piDelivered);
                        }
                    }
                } else {
                    Toast.makeText(context,"Ni rezov",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Reservation>> call, Throwable t) {
                Toast.makeText(context,"FAILOV GET REQUEST",Toast.LENGTH_LONG).show();
            }
        });
    }
}
