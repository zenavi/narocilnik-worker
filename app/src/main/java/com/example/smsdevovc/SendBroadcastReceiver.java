package com.example.smsdevovc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SendBroadcastReceiver extends BroadcastReceiver {
    private final String DEBUG_TAG = getClass().getSimpleName().toString();
    private static final String ACTION_SMS_SENT = "SMS_SENT";

    // When the SMS has been sent
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (action.equals(ACTION_SMS_SENT)) {

            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(context, "SMS Sent", Toast.LENGTH_SHORT).show();
                    Bundle b = intent.getExtras();
                    if (b != null) {
                        String value = b.getString("id_sent");
                        Log.e("SENT_ID","Sent sms for reservation: " + value);
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://43c87ce1.ngrok.io")
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        //api instanca
                        IJsonMobileAPI api = retrofit.create(IJsonMobileAPI.class);
                        String repValue = value.replace("\\s","");
                        String skp =  "{\"id\":\"" + repValue + "\"}" ;
                        JsonObject jsonObj = new JsonParser().parse(skp).getAsJsonObject();
                        Call<JsonObject> klic = api.postIdMobile(jsonObj);

                        klic.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if(!response.isSuccessful()){
                                    Log.e("LOGICFAIL", "onResponse not successful: " + response );
                                }

                                JsonObject postResoponse = response.body();
                                Log.e("LOGICOK","Post response: " + postResoponse);
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Log.e("ONNAPAKA", t.toString());
                            }
                        });
                    }
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(context, "Generic failure", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(context, "No service", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(context, "Null PDU", Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(context, "Radio off", Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

}