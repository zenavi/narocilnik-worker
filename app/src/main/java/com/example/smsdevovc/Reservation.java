package com.example.smsdevovc;

import com.google.gson.annotations.SerializedName;

public class Reservation {

    @SerializedName("_id")
    private String id;
    @SerializedName("phone")
    private String phone;
    @SerializedName("avtSms")
    private String avtSms;

    public String getAvtSms() { return avtSms; }

    public String getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }


}
